//Function
/*
	Syntax:
			function functionName(){
				code block (statement)
			};

*/
function printname(){
	console.log("My name is Lei");
};

printname();

//Function declarations vs expressions

// Function declaration
function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression
let varaibleFunction = function(){
	console.log("Hello again!");
};

varaibleFunction();

declaredFunction = function() {
	console.log("Updated declaredFunction")
};

declaredFunction();

// const constantFunc = function(){
// 	console.log("Initialized width const!")
// };

// constantFunc = function(){
// 	console.log("Cannot be re-assigned");
// };

// constantFunc();
/*Print Error*/

//Function scoping 

/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/
// Inside in code block { let~ }
{
	let localVar = "Alonzo Mattheo";
	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames() {
	// Function scoped varaibles
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}

myNewFunction();

//Function and Global scoped varaibles

// Global scoped variable 

let globalName = "Joy";

function myNewFunction2(){
	let nameInside = "Kenzo";

	console.log(globalName);
	console.log(nameInside);
}

myNewFunction2();

// console.log(nameInside);

//Using alert()

alert("Hello, World!"); // This wil be run immediately when the page loads

function showSampleAlert(){
	alert("Hello, user!");
}

showSampleAlert();

// Using prompt()

let samplePrompt = prompt(" Enter you name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter you first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

/*
Function ]naming conventions
	- Function names should be definitive of the task it will perform
	- Avoid generic names to avoid confusion within the code
	- Avvoid pointless and inappropriate function names
	- Name you functionsyour functions following camel casing
	- Do not use JS reserved keywords
*/
console.log("Hello, World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
		let firstName = prompt("Enter you first name: ");
		let lastName = prompt("Enter your last name: ");
		let age = prompt("Enter your age: ");
		let place = prompt(" Enter your place: ");
		console.log("You are " + age + " years old.");
		console.log("You live in " + place+ " " + "City.");
	}

	printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:
	
	let artist1 = "1. The Beatles";
	let artist2 = "2. Metallica";
	let artist3 = "3. The Eagles";
	let artist4 = "4. L'arc~en~Ciel";
	let artist5 = "5. Eraserheads";

	function myNewFunction2(){

		console.log(artist1);
		console.log(artist2);
		console.log(artist3);
		console.log(artist4);
		console.log(artist5);
	}
	myNewFunction2();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//third function here:
	let firstMovie = "The GodFather";
	let secondMovie = "The GodFather, Part II";
	let thirdMovie = "Shawshank Redemption";
	let fourthMovie = "To Kill A Mockingbird";
	let fifthMovie = "Psycho";

	function myNewFunction3(){

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Rating: 98%");
		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Rating: 93%");
		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Rating: 90%");
	}
	myNewFunction3();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// let printFriends() = 
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name: "); 
	let friend2 = prompt("Enter your second friend's name: "); 
	let friend3 = prompt("Enter your third friend's name: ");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();
// console.log(friend1);
// console.log(friend2);
